import sys
from PolybiusCipher import PolybiusCipher

if __name__ == '__main__':
    cipher = PolybiusCipher()

    text = sys.argv[2]
    if isinstance(text, bytes):
        text = text.decode('utf8')

    if sys.argv[1] == 'encrypt':
        encrypted = cipher.encrypt(sys.argv[2])
        print(encrypted)
    elif sys.argv[1] == 'decrypt':
        try:
            decrypted = cipher.decrypt(sys.argv[2])
            print(decrypted)
        except:
            print('Cannot decrypt text ' + text)
    else:
        print('Usage: python main.py (encrypt|decrypt) message')
