#!/usr/bin/env python
# -*- coding: utf-8 -*-

from unittest import TestCase
from PolybiusCipher import PolybiusCipher


class TestPolybiusCipher(TestCase):
    def test_encrypt(self):
        cipher = PolybiusCipher()

        text = u'привет, амиран'
        expected = u'вгвдбваваегаегееаавабввдаавб'
        actual = cipher.encrypt(text)

        self.assertEqual(expected, actual)

    def test_decrypt(self):
        text = u'вгвдбваваегаегееаавабввдаавб'
        expected = u'привет, амиран'
        actual = PolybiusCipher.decrypt(text)
        self.assertEqual(expected, actual)

    def test_decrypt_failure(self):
        self.assertRaises(Exception, PolybiusCipher.decrypt, u'вгв')
        self.assertRaises(Exception, PolybiusCipher.decrypt, u'hi')
