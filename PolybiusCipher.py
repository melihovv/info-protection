#!/usr/bin/env python
# -*- coding: utf-8 -*-


class PolybiusCipher:
    dict = {
        u'а': u'аа',
        u'б': u'аб',
        u'в': u'ав',
        u'г': u'аг',
        u'д': u'ад',
        u'е': u'ае',

        u'ж': u'ба',
        u'з': u'бб',
        u'и': u'бв',
        u'й': u'бг',
        u'к': u'бд',
        u'л': u'бе',

        u'м': u'ва',
        u'н': u'вб',
        u'о': u'вв',
        u'п': u'вг',
        u'р': u'вд',
        u'с': u'ве',

        u'т': u'га',
        u'у': u'гб',
        u'ф': u'гв',
        u'х': u'гг',
        u'ц': u'гд',
        u'ч': u'ге',

        u'ш': u'да',
        u'щ': u'дб',
        u'ъ': u'дв',
        u'ы': u'дг',
        u'ь': u'дд',
        u'э': u'де',

        u'ю': u'еа',
        u'я': u'еб',
        u'.': u'ев',
        u',': u'ег',
        u'-': u'ед',
        u' ': u'ее',
    }

    @staticmethod
    def encrypt(text):
        encrypted = ''

        for char in text:
            if char in text:
                encrypted += PolybiusCipher.dict[char]
            else:
                raise Exception('Illegal symbol ' + char)

        return encrypted

    @staticmethod
    def decrypt(text):
        decrypted = ''
        reversed_dict = {v: k for k, v in PolybiusCipher.dict.items()}

        size = len(text)
        for i, char in enumerate(text[0::2]):
            index_next = 2 * i + 1

            if index_next < size:
                pair = char + text[index_next]
            else:
                raise Exception('Cannot decode text ' + text)

            if pair in reversed_dict:
                decrypted += reversed_dict[pair]
            else:
                raise Exception('Illegal symbols ' + char + text[i + 1])

        return decrypted
